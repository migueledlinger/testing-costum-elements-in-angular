import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Costum-Elements';

  _state = "enabled";

  get state() {

    return this._state;
  }

  set state(s: string) {

    this._state = s;  
  }

  transformableText = new FormControl('');
  get text() {
    // console.log(this.transformableText.value); 
    return this.transformableText.value;
  }


  getUpdate() {
    const plt = document.querySelector("plain-text");
    // this._state = plt.getState(); 
  }


}